#!/usr/bin/env python

import rospy
from std_msgs.msg import Int16

def MotorPublisher(motor_number, motor):
    motor_message = Int16()
    # motor_message.data = input("Valeur moteur " + str(motor_number) + " : ")
    motor_message.data = 10
    rospy.loginfo("Motor " + str(motor_number) + " value : " + str(motor_message.data))
    motor.publish(motor_message)

def MotorTalker():
    motor1_publisher = rospy.Publisher('/motor1', Int16, queue_size=10)
    motor2_publisher = rospy.Publisher('/motor2', Int16, queue_size=10)
    motor3_publisher = rospy.Publisher('/motor3', Int16, queue_size=10)
    motor4_publisher = rospy.Publisher('/motor4', Int16, queue_size=10)
    motor5_publisher = rospy.Publisher('/motor5', Int16, queue_size=10)
    motor6_publisher = rospy.Publisher('/motor6', Int16, queue_size=10)
    motor7_publisher = rospy.Publisher('/motor7', Int16, queue_size=10)
    rospy.init_node('motor_node', anonymous=True)
    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        MotorPublisher(1, motor1_publisher)
        MotorPublisher(2, motor2_publisher)
        # MotorPublisher(3, motor3_publisher)
        # MotorPublisher(4, motor4_publisher)
        # MotorPublisher(5, motor5_publisher)
        # MotorPublisher(6, motor6_publisher)
        # MotorPublisher(7, motor7_publisher)
        rate.sleep()

if __name__ == '__main__':
    try:
        MotorTalker()
    except rospy.ROSInterruptException:
        pass
