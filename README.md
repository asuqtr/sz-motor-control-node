# README #

This repo is about the motor node of the sub-zero.


### SUMMARY ###

* Connects to the Rosbridge server via UDP Ethernet
* Subscribes to each motor's topic
* Deserialises every JSON data it receives and applies it to the proper motor

### REPO OWNER ###

Admin : Nicolas Toupin
Contributors : 
- Maxime Verreault


### SET-UP ###

* To make this node work, connect it to the same network as ROS master. Connect also Arduino to motor driver board.
* Must download Arduino libraries:
*	-https://github.com/bblanchon/ArduinoJson


### UNIT TESTS ###

Unit test 1: test-motor (sweep between -25% and 25% of motor speed)
