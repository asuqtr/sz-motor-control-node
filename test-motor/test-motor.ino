// Motor: Max forward: 1900 microseconds, Max reverse: 1100 microseconds and Stopped: 1500 microseconds (http://docs.bluerobotics.com/besc/)
// Motor: Min: 1100 microseconds and Max: 1900 microseconds (http://docs.bluerobotics.com/lumen/)

#include <Servo.h>

const int motor1_pin = 3;
const int motor2_pin = 4;
const int motor3_pin = 5;
const int motor4_pin = 6;
const int motor5_pin = 7;
const int motor6_pin = 8;
const int motor7_pin = 9;

const int light1_pin = 10;
const int light2_pin = 11;

Servo motor1_obj;
Servo motor2_obj;
Servo motor3_obj;
Servo motor4_obj;
Servo motor5_obj;
Servo motor6_obj;
Servo motor7_obj;

Servo light1_obj;
Servo light2_obj;

void setup() {

  AttachHardware();

  Serial.println("Configuration stared");
  digitalWrite(LED_BUILTIN, LOW);

  SetAllMotorSpeed(0);
  SetAllLightIntensity(0);

  delay(1000);

  Serial.println("Configuration ended");
  digitalWrite(LED_BUILTIN, HIGH);
}


void loop() {

  // Sweep Motor Values
  for (int i = -25; i <= 25; i++) {
    SetAllMotorSpeed(i);
    delay(100);
  }
  for (int i = 25; i >= -25; i--) {
    SetAllMotorSpeed(i);
    delay(100);
  }

  // Sweep Light Values
  for (int i = 0; i <= 100; i++) {
    SetAllLightIntensity(i);
    delay(100);
  }
  for (int i = 100; i >= 0; i--) {
    SetAllLightIntensity(i);
    delay(100);
  }
}


void AttachHardware() {
  motor1_obj.attach(motor1_pin);
  motor2_obj.attach(motor2_pin);
  motor3_obj.attach(motor3_pin);
  motor4_obj.attach(motor4_pin);
  motor5_obj.attach(motor5_pin);
  motor6_obj.attach(motor6_pin);
  motor7_obj.attach(motor7_pin);

  light1_obj.attach(light1_pin);
  light2_obj.attach(light2_pin);

  Serial.begin(9600);

  pinMode(LED_BUILTIN, OUTPUT);
}

void UpdateMotorSpeed(String motor_name, Servo motor, int speed_setpoint) {

  double motor_setpoint;
  motor_setpoint = map(speed_setpoint, -100, 100, 1100, 1900);

  Serial.print("Setting ");
  Serial.print(motor_name);
  Serial.print(" intensity to ");
  Serial.print(speed_setpoint);
  Serial.print(" % (");
  Serial.print((int)motor_setpoint);
  Serial.println(")");

  motor.writeMicroseconds((int)motor_setpoint);
}

void SetAllMotorSpeed(int speed_setpoint) {

  UpdateMotorSpeed("Motor 1", motor1_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 2", motor2_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 3", motor3_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 4", motor4_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 5", motor5_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 6", motor6_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 7", motor7_obj, speed_setpoint);
}

void UpdateLightIntensity(String light_name, Servo light, int intensity_setpoint) {

  double light_setpoint;
  light_setpoint = map(intensity_setpoint, 0, 100, 1100, 1900);

  Serial.print("Setting light ");
  Serial.print(light_name);
  Serial.print(" intensity to ");
  Serial.print(intensity_setpoint);
  Serial.print(" % (");
  Serial.print((int)light_setpoint);
  Serial.println(")");

  light.writeMicroseconds((int)light_setpoint);
}

void SetAllLightIntensity(int light_setpoint) {

  UpdateLightIntensity("Light 1", light1_obj, light_setpoint);
  UpdateLightIntensity("Light 2", light2_obj, light_setpoint);
}

