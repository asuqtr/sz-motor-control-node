// Motor: Max forward: 1900 microseconds, Max reverse: 1100 microseconds and Stopped: 1500 microseconds (http://docs.bluerobotics.com/besc/)
// Motor: Min: 1100 microseconds and Max: 1900 microseconds (http://docs.bluerobotics.com/lumen/)

#include <Servo.h>
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <ArduinoJson.h>

// Ethernet variables
byte MacAddress[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress MyIPAddress(192, 168, 1, 91);
unsigned int LocalPort = 9393;
IPAddress DestinationAddress(192, 168, 1, 16);
unsigned int DestinationPort = 9393;
EthernetUDP Udp;
char packetBuffer[128];

// I/O Variables
const int motor1_pin = 2;
const int motor2_pin = 3;
const int motor3_pin = 4;
const int motor4_pin = 5;
const int motor5_pin = 6;
const int motor6_pin = 7;
const int motor7_pin = 8;
const int light1_pin = 0;
const int light2_pin = 9;

// PWM Variables
Servo motor1_obj;
Servo motor2_obj;
Servo motor3_obj;
Servo motor4_obj;
Servo motor5_obj;
Servo motor6_obj;
Servo motor7_obj;
Servo light1_obj;
Servo light2_obj;

void setup() {

  Serial.begin(9600);

  AttachHardware();

  Serial.println("Configuration stared");
  digitalWrite(LED_BUILTIN, LOW);

  SetAllMotorSpeed(0);
  SetAllLightIntensity(0);

  // Ethernet Setup
  Ethernet.begin(MacAddress, MyIPAddress);
  Udp.begin(LocalPort);
  Serial.print("Local IP : ");
  Serial.print(MyIPAddress);
  Serial.print(":");
  Serial.println(LocalPort);
  Serial.print("Destination IP : ");
  Serial.print(DestinationAddress);
  Serial.print(":");
  Serial.println(DestinationPort);

  // Subscribe to Motor 1
  DynamicJsonBuffer jsonBuffer_M1(200);
  JsonObject& root_M1 = jsonBuffer_M1.createObject();
  root_M1["op"] = "subscribe";
  root_M1["topic"] = "/motor1";
  Serial.print("JSON String : ");
  root_M1.printTo(Serial);
  Serial.println();
  char jsonChar_M1[100];
  root_M1.printTo((char*)jsonChar_M1, root_M1.measureLength() + 1);
  Udp.beginPacket(DestinationAddress, DestinationPort);
  Udp.write(jsonChar_M1);
  Udp.endPacket();

  delay(100);

  // Subscribe to Motor 2
  DynamicJsonBuffer jsonBuffer_M2(200);
  JsonObject& root_M2 = jsonBuffer_M1.createObject();
  root_M2["op"] = "subscribe";
  root_M2["topic"] = "/motor2";
  Serial.print("JSON String : ");
  root_M2.printTo(Serial);
  Serial.println();
  char jsonChar_M2[100];
  root_M2.printTo((char*)jsonChar_M2, root_M2.measureLength() + 1);
  Udp.beginPacket(DestinationAddress, DestinationPort);
  Udp.write(jsonChar_M2);
  Udp.endPacket();

  Udp.begin(LocalPort);

  Serial.println("Configuration ended");
  digitalWrite(LED_BUILTIN, HIGH);
}


void loop() {

  int packetSize = Udp.parsePacket();

  if (packetSize) {
    Serial.println(packetSize);

    Udp.read(packetBuffer, 128);
    Serial.println(packetBuffer);

    // Read
    DynamicJsonBuffer  jsonBuffer(200);
    JsonObject& root = jsonBuffer.parseObject(packetBuffer);
    const char* moteur = root["topic"];
    int speed_setpoint = root["msg"]["data"];
    Serial.println(speed_setpoint);

    // Parse motor value
    if (strcmp(moteur, "/motor1") == 0) {
      UpdateMotorSpeed("Motor 1", motor1_obj, speed_setpoint);
    }
  }

  delay(100);

  /*if (packetSize) {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remote = Udp.remoteIP();
    for (int i = 0; i < 4; i++) {
      Serial.print(remote[i], DEC);
      if (i < 3) {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    //Udp.read(packetBuffer, 128);

    // Read
    DynamicJsonBuffer  jsonBuffer(200);
    JsonObject& root = jsonBuffer.parseObject(packetBuffer);
    const char* moteur = root["topic"];
    int speed_setpoint = root["data"];
    //Serial.println(moteur);
    Serial.println(packetBuffer);
    //Attribution de la consigne au bon moteur
    if (strcmp(moteur, "M1") == 0) {
      SetAllMotorSpeed(speed_setpoint);
      Serial.println(speed_setpoint);
      //UpdateMotorSpeed("Motor 1", motor1_obj, speed_setpoint);
    }

    }
  */
}


void AttachHardware() {
  motor1_obj.attach(motor1_pin);
  motor2_obj.attach(motor2_pin);
  motor3_obj.attach(motor3_pin);
  motor4_obj.attach(motor4_pin);
  motor5_obj.attach(motor5_pin);
  motor6_obj.attach(motor6_pin);
  motor7_obj.attach(motor7_pin);

  light1_obj.attach(light1_pin);
  light2_obj.attach(light2_pin);

  Serial.begin(9600);

  pinMode(LED_BUILTIN, OUTPUT);
}

void UpdateMotorSpeed(String motor_name, Servo motor, int speed_setpoint) {

  double motor_setpoint;
  motor_setpoint = map(speed_setpoint, -100, 100, 1100, 1900);

  Serial.print("Setting ");
  Serial.print(motor_name);
  Serial.print(" intensity to ");
  Serial.print(speed_setpoint);
  Serial.print(" % (");
  Serial.print((int)motor_setpoint);
  Serial.println(")");

  motor.writeMicroseconds((int)motor_setpoint);
}

void SetAllMotorSpeed(int speed_setpoint) {

  UpdateMotorSpeed("Motor 1", motor1_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 2", motor2_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 3", motor3_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 4", motor4_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 5", motor5_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 6", motor6_obj, speed_setpoint);
  UpdateMotorSpeed("Motor 7", motor7_obj, speed_setpoint);
}

void UpdateLightIntensity(String light_name, Servo light, int intensity_setpoint) {

  double light_setpoint;
  light_setpoint = map(intensity_setpoint, 0, 100, 1100, 1900);

  Serial.print("Setting light ");
  Serial.print(light_name);
  Serial.print(" intensity to ");
  Serial.print(intensity_setpoint);
  Serial.print(" % (");
  Serial.print((int)light_setpoint);
  Serial.println(")");

  light.writeMicroseconds((int)light_setpoint);
}

void SetAllLightIntensity(int light_setpoint) {

  UpdateLightIntensity("Light 1", light1_obj, light_setpoint);
  UpdateLightIntensity("Light 2", light2_obj, light_setpoint);
}

